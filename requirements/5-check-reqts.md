<treqs>
<treqs-element id="c5402c1a919411eb8311f018989356c1" type="requirement">

### 5.0 Parameters and default output of treqs check

    Usage: treqs check [OPTIONS]
    
      Check treqs elements in this folder
    
    Options:
      --recursive TEXT  List treqs elements recursively in all subfolders.
      --filename TEXT   Give a file or directory to list from.
      --ttim TEXT       Path to a type and traceability information model (TTIM) in json format.
      --help            Show this message and exit.

The default value for recursive is 'true'. Unless otherwise specified, treqs shall list elements recursively.

Filename can either provide a directory or a file. If filename is omitted, treqs defaults to '.', i.e. the current working directory.

TTIM points to a json file that contains a type and trace information model (TTIM). The default is ./ttim.json. Currently, the only relevant information in the TTIM file is an array containing all types and their relationships. Consult the sample file ttim.json for a usage example.
</treqs-element>

<treqs-element id="3cff0d2a919511eb8becf018989356c1" type="requirement">

### 5.1 Check for unrecognised types.

When checking treqs elements, treqs shall report elements that have types not listed in the TTIM. Additionally, treqs shall report the file in which this violation occured.

> Example:

VIOLATION: Unrecognized type: SR (File ./requirements/treqs-system-requirements.md)

<treqs-link type="hasParent" target="c5402c1a919411eb8311f018989356c1" />
</treqs-element>

<treqs-element id="951ecc70919511eb978ff018989356c1" type="requirement">

### 5.2 Check for unrecognised link types.

When checking treqs links, treqs shall report links that have types not listed for the parent element in the TTIM. Additionally, treqs shall report the file in which this violation occured.

> Example:

VIOLATION: Unrecognized link type testss within an element of type unittest (File ./requirements/test_req.md)
<treqs-link type="hasParent" target="c5402c1a919411eb8311f018989356c1" />
</treqs-element>

<treqs-element id="b4d30bec919711eba4e1f018989356c1" type="requirement">

### 5.3 Check for missing links.

When checking treqs elements, treqs shall report links that are required according to the TTIM, but missing in the element. Additionally, treqs shall report the file in which this violation occured.

> Example:

VIOLATION: Links missing for ec5d1aa4915511eb9295f018989356c1: ['tests'] (File ./requirements//test_req.md)
<treqs-link type="hasParent" target="c5402c1a919411eb8311f018989356c1" />
</treqs-element>
</treqs>