<treqs>

# TReqs requirements

## Conventions

TReqs relies on markdown to render requirements. In order to allow tool support, metadata is provided as xml elements and attributes. This file provides examples for this. 

The first none-empty textline of a treqs element is regarded as its label. It should therefore be a short characterisation of the requirement, allowing to identify the requirement in context, but still fit easily in one line on the screen.


<treqs-element id="97a8fb92-9613-11ea-bb37-0242ac130002" type="requirement" >

## 1. Create TReqs Elements
TReqs shall be able to create treqs-elements 

> Examples:
> Minimal example (returns a treqs-element with a newy generated id of the specified type and prints it on commandline)

    treqs create --type=requirement

> Specify a file to append the new element automatically (may be useful if called from within a tool) **(TODO)**   

    treqs create --type=requirement --targetfile="requirements/SR_requirements.md" --content="text of new requirement\n may have several lines"

Suggestion:

- Have a function that returns text to the terminal/console, which then can be copied and pasted into a markdown file or header of an automated test
- Allow to specify a file to which the new element should be appended
- Such a function could take optional paramenters, such as type and test. If those are missing, just an empty treqs-element structure is returned.

</treqs-element>

<treqs-element id="35590bca-960f-11ea-bb37-0242ac130002" type="requirement" >

## 2. List TReqs Elements
TReqs shall be able to list treqs-elements within a specified gitlab project.

> Example:

    treqs list

</treqs-element>


<treqs-element id="c153021087f011eb8a15c4b301c00591" type="requirement">

## 4. Process treqs files

TReqs shall be able to process files in the following way:

- Create and link image from embedded PlantUML


</treqs-element>

----



# A few more rough thoughts (EK)

## consistency aspects
- The functions specfied above would create a common infrastructure. In addition, it should be possible to get all tracelinks to and from a treqs-element.
- In addition, it should be possible to store within a gitlab project 
   - a requirements information model (i.e. which treqs-elements are allowed in this gitlab-project) and 
   - a traceability information model (i.e. which types of links are allowed/required between treqs-elements)
- since that might be difficult, we might go for a different approach
   - since the convenience functions above provide good infrastructure, the gitlab project could store instead consistency rules
   - such consistency rules are written in python and executed by the treqs command
   - a rule can _fire_, i.e. add a line to a log consisting of criticality, location, message, description (including suggestions for fixing)
   - rules could for example check whether mandatory tracelinks exist. 
   - they could also check certain conventions or writing rules (e.g. avoiding weakwords, passive voice, ...)
   
It should be a good practice to check consistency before pushing, thus, these rules resemble unit tests.
   
## Replacement rules

In addition to consistency, we have a lot of things where treqs should add automatically generated stuff to a markdown file.

- Mebrahtom's approach to modeling, where a script generates a line that calls the plantuml webservice for generating a picture of each plantuml diagram
- One could generate some markdown text with links to elements

Those should, of course, be marked somehow, since the next execution should overwrite them. 

It is conceivable to automatically execute those on commit or push.



## test requirement tag
<treqs-element id="022d76a89d2211eb9cc9f018989356c1" type="requirement">

<b>Requirement 1</b>
This is the content of requirement</treqs-element>


<treqs-element id="0b7b89709d2211eb9d31f018989356c1" type="requirement"> 

<b>Requirement 1.1</b>
this is the content of requirement1.1
<treqs-link type="hasParent" target="022d76a89d2211eb9cc9f018989356c1" />

</treqs-element>

<treqs-element id="133142cc9d2211ebb3d6f018989356c1" type="requirement" link_element="root" email="person1" date="20200101 00:00:00">

<b>SR1</b>

System Requirement 1 - currently just type requirement.
</treqs-element>

<treqs-element id="0e84ec149d2311eb845df018989356c1" type="requirement" email="XXXXX" date="20200000 00:00:00">

<b>SR1.1</b>

System Requirement 1.1 - currently just type requirement.
<treqs-link type="hasParent" target="133142cc9d2211ebb3d6f018989356c1" />
</treqs-element>

<treqs-element id="1b3713ce9d2311eb93fbf018989356c1" type="requirement" link_element="root" email="person1" date="20200101 00:00:00">

<b>SR2</b>
</treqs-element>

<treqs-element id="21d4de0a9d2311eb9dd1f018989356c1" type="requirement" email="person1" date="20200101 00:00:00">

<b>SR2.1</b>

<treqs-link type="hasParent" target="1b3713ce9d2311eb93fbf018989356c1" />
</treqs-element>

</treqs>
