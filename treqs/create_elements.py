import uuid

class create_elements:

    def create_markdown_element(tet, label):
        uid = str(uuid.uuid1().hex)
        result = '<treqs-element id="'+uid+'" type="' + tet + '">'
        result += '\n'
        result += '\n'
        result += label
        result += '\nadd additional information/details here'
        result += '\n</treqs-element>'

        return result

    def create_link(lt, target):
        result = '<treqs-link type="' + lt + '" target="' + target + '" />'
        return result
